package com.hcmute.starter.controller;

import com.hcmute.starter.mapping.UserMapping;
import com.hcmute.starter.model.entity.UserEntity;
import com.hcmute.starter.model.payload.SuccessResponse;
import com.hcmute.starter.model.payload.request.AddNewUserRequest;
import com.hcmute.starter.model.payload.response.ErrorResponseMap;
import com.hcmute.starter.security.JWT.JwtUtils;
import com.hcmute.starter.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.UUID;

import static com.google.common.net.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping("api/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    JwtUtils jwtUtils;
    //Get user by id
    //This request is: http://localhost:8080/api/user/{id}
    @GetMapping("{id}")
    public ResponseEntity<SuccessResponse> getUserByID(@PathVariable("id")String id) {
        UserEntity user=userService.findById(UUID.fromString(id));
        SuccessResponse response=new SuccessResponse();
        if(user==null){
            response.setMessage("User not found");
            response.setStatus(HttpStatus.NOT_FOUND.value());
            response.setSuccess(false);
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);

        }
        else{
            response.setMessage("Get user success");
            response.setSuccess(true);
            response.setStatus(HttpStatus.OK.value());
            response.getData().put("user",user);
            return new ResponseEntity<>(response, HttpStatus.OK);

        }

    }
    //Get all user
    //This request is: http://localhost:8080/api/user/lits
    @GetMapping("/profile")
    public ResponseEntity<SuccessResponse> getAllUser(HttpServletRequest request) throws Exception{
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            String accessToken = authorizationHeader.substring("Bearer ".length());
            if(jwtUtils.validateExpiredToken(accessToken) == true){
                throw new BadCredentialsException("access token is  expired");
            }
            UserEntity user=userService.findByPhone(jwtUtils.getUserNameFromJwtToken(accessToken));
            if(user==null){
                throw new BadCredentialsException("User not found");
            }
            else{
                SuccessResponse response=new SuccessResponse();
                response.setMessage("Get user success");
                response.setSuccess(true);
                response.setStatus(HttpStatus.OK.value());
                response.getData().put("user",user);
                return new ResponseEntity<>(response, HttpStatus.OK);
            }


        }
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);


    }
    @PostMapping("/register")
    public ResponseEntity<SuccessResponse> registerUser(@RequestBody @Valid AddNewUserRequest request) {
        UserEntity user= UserMapping.registerToEntity(request);
        if(userService.existsByPhone(user.getPhone())){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        SuccessResponse response=new SuccessResponse();
        try{
            userService.saveUser(user,"USER");
            response.setStatus(HttpStatus.OK.value());
            response.setMessage("add user successful");
            response.setSuccess(true);
            response.getData().put("email",user.getEmail());
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(response, HttpStatus.OK);

    }

}
